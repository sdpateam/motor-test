/***
   
    M O T O R    E V A L U A T I O N

    SDP2016 Group 1

***/

#include "SerialCommand.h"
#include "SDPArduino.h"
#include <Wire.h>
#include <Arduino.h>

#define FW_DEBUG                             // Comment out to remove serial debug chatter

#define sensorAddr 0x39  // Sensor physical address on the power board - 0x39
#define ch0        0x43  // Read ADC for channel 0 - 0x43
#define ch1        0x83  // Read ADC for channel 1 - 0x83

#define encoder_i2c_addr 5

/***
    STRUCTURES & PROTOTYPES
***/

struct state {
    int battery, status_led, status_led_pin;
    float heading;
};

struct state self;

struct motor {
    int port, encoder, power, direction;
    float correction, speed;
};

struct motor test_mtr;

/* 
   Define an array of our drive motors,
   this generalises the motor control logic

   NB this is *not* itself a struct
*/
const int num_drive_motors = 1;
struct motor* driveset[num_drive_motors] = {
    &test_mtr
};

struct sensor {
    int i2c_addr, last_value;
};

struct sensor encoders;

// Rotary encoders
int positions[num_drive_motors] = {0};


/***
    LOOP & SETUP
***/
void setup()
{
    self.status_led_pin = 13;
    pinMode(self.status_led_pin, OUTPUT);
    digitalWrite(self.status_led_pin, self.status_led);

    SDPsetup();

    Serial.begin(115200);
    Serial.println("Motor evaluation init done");

    /* set up main drive motors */
    test_mtr.port = 5;
    test_mtr.encoder = 0;
    test_mtr.direction = 1;
    test_mtr.power = 0;
    test_mtr.correction = 0.0;
    test_mtr.speed = 0.0;
    delay(10);

    /**
       RUN TEST
    **/

    float speeds[16];
    int spd_index = 0;
    
    int speed = 100;
    Serial.print("Starting test for motor on port ");
    Serial.println(test_mtr.port);

    while (speed >= 30){
        speeds[spd_index++] = test(speed);
        speed -= 10;
    }
    speed = -100;
    while (speed <= -30){
        speeds[spd_index++] = test(speed);
        speed += 10;
    }

    motorStop(test_mtr.port);
    Serial.println("Done");

    for (int i=0; i<spd_index; i++){
        Serial.print(speeds[i]);
        Serial.print("\t");
    }
    Serial.println();
    
}

void loop() 
{ 
    // Do nothing after test
}

float test(int speed)
{
    Serial.print(speed);
    Serial.print(" gives ");
    
    if (speed >= 0)
        motorForward(test_mtr.port, speed);
    else
        motorBackward(test_mtr.port, abs(speed));

    Wire.requestFrom(encoder_i2c_addr, 1);
    byte stops = (byte) Wire.read();
    delay(1500); // wait for motor to spin up

    /* Remember start time */
    unsigned long tstart = millis();
    unsigned int total_stops = 0;
    int iters = 15;

    /* keep checking encoders */
    for (int i=0; i<iters; i++){
        Wire.requestFrom(encoder_i2c_addr, 1);
        stops = (byte) Wire.read();
        stops = speed < 0 ? 0xFF - stops : stops;
        total_stops += stops;
        delay(100);
    }
    
    unsigned long tdiff = millis() - tstart;
    float rot_speed = (float) total_stops / ((float) tdiff / 1000.0);

    Serial.print(rot_speed);
    Serial.print(" stops/sec (");
    Serial.print(total_stops);
    Serial.print(" stps/");
    Serial.print(tdiff);
    Serial.print(" ms)");
    Serial.println();

    return rot_speed;
}

/***
    FUNCTIONS
***/
void updateMotorPositions()
{
    /* Request motor position deltas from encoder board */
    Wire.requestFrom(encoder_i2c_addr, num_drive_motors);
    for (int i = 0; i < num_drive_motors; i++) {
        positions[i] = (int8_t) Wire.read();
    }
}

void printMotorPositions()
{
    Serial.print("Encoders: ");
    for (int i = 0; i < num_drive_motors; i++) {
        Serial.print(positions[i]);
        Serial.print(' ');
    }
    Serial.println();
}

void getenc()
{
    updateMotorPositions();
    printMotorPositions();
}


// Function to stop specific motor, also sets the power to 0
void motor_stop(struct motor* m) 
{
    m->power = 0;
    motorStop(m->port);
}

/* Force stops all motors */
void all_stop()
{
    #ifdef FW_DEBUG
    Serial.println("Force stopping");
    #endif
    
    for (int i=0; i < num_drive_motors; i++){
        driveset[i]->power = 0;
    }
    motorAllStop();
}